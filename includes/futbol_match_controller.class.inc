<?php

/**
 * @file
 * Controller for the Futbol Match entity.
 */

/**
 * The Controller for Futbol Match entities.
 */
class FutbolMatchController extends EntityAPIController {

  /**
   * This ensures that the result is loaded into the match.
   */
  protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
    $query = parent::buildQuery($ids, $conditions, $revision_id);
    // Left join to the results table to get any results.
    $query->leftJoin('futbol_result', 'fr', 'base.fid = fr.fid');
    $query->addField('fr', 'home_goals', 'home_goals');
    $query->addField('fr', 'away_goals', 'away_goals');
    return $query;
  }

  /**
	 * Save to the results table if necessary.
	 */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    // Save the entity as normal.
    $save = parent::save($entity, $transaction);
    // Then check to see if we have a result, and save it if we do.
    if ((!empty($entity->home_goals) || $entity->home_goals === 0) && (!empty($entity->away_goals) || $entity->away_goals === 0)) {
      $table = 'futbol_result';
      $record = array(
        'fid' => $entity->fid,
        'home_goals' => $entity->home_goals,
        'away_goals' => $entity->away_goals,
      );
    }
    drupal_write_record($table, $record);
  }

}
