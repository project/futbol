<?php

/**
 * @file
 * Admin functions for the Futbol module.
 */

/**
 * Generates the team editing form.
 */
function futbol_team_form($form, &$form_state, $futbol_team, $op = 'edit') {
  $form['full_name'] = array(
    '#title' => t('Full team name'),
    '#description' => t("For example, Barça's full name is Futbol Club Barcelona."),
    '#type' => 'textfield',
    '#default_value' => isset($futbol_team->full_name) ? $futbol_team->full_name : '',
  );
  $form['short_name'] = array(
    '#title' => t('Short team name'),
    '#description' => t('FC Barcelona would be the short name for Barça.'),
    '#type' => 'textfield',
    '#default_value' => isset($futbol_team->short_name) ? $futbol_team->short_name : '',
  );
  $form['common_name'] = array(
    '#title' => t('Common team name'),
    '#description' => t('The common name for Barça is simply Barcelona.'),
    '#type' => 'textfield',
    '#default_value' => isset($futbol_team->common_name) ? $futbol_team->common_name : '',
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save team'),
  );
  return $form;
}

/**
 * Submit callback for the team form.
 */
function futbol_team_form_submit(&$form, &$form_state) {
  $futbol_team = entity_ui_form_submit_build_entity($form, $form_state);
  $futbol_team->save();
  drupal_set_message(t('Team %team added.', array('%team' => $futbol_team->full_name)));
  $form_state['redirect'] = 'admin/futbol/team';
}

/**
 * Loads an Futbol team by the team ID.
 */
function futbol_team_load($tid = NULL, $reset = FALSE) {
  $tids = (isset($tid) ? array($tid) : array());
  $team = futbol_team_load_multiple($tids, $reset);
  return $team ? reset($team) : FALSE;
}

/**
 * Loads multiple teams by ID or based on a set of matching conditions.
 *
 * @see entity_load()
 *
 * @param $tids
 *   An array of team IDs.
 * @param $conditions
 *   An array of conditions on the futbol_team table in the form
 *     'field' => $value.
 * @param $reset
 *   Whether to reset the internal team loading cache.
 *
 * @return
 *   An array of team objects indexed by tid.
 */
function futbol_team_load_multiple($tids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('team', $tids, $conditions, $reset);
}

/**
 * Generates the form for the Futbol match entity.
 */
function futbol_match_form($form, &$form_state, $futbol_match, $op = 'edit') {
  $form['team_details'] = array(
    '#title' => t('Team details'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['team_details']['home_team_id'] = array(
    '#title' => t('Home team'),
    '#description' => t("The match's home team."),
    '#type' => 'textfield',
    '#autocomplete_path' => 'futbol/team/autocomplete',
    '#required' => TRUE,
    '#default_value' => isset($futbol_match->home_team_id) ? futbol_team_autocomplete_val($futbol_match->home_team_id) : '',
  );
  $form['team_details']['home_goals'] = array(
    '#title' => t('Home goals'),
    '#description' => t('The number of goals scored by the home team.'),
    '#type' => 'textfield',
    '#default_value' => isset($futbol_match->home_goals) ? $futbol_match->home_goals : '',
  );
  $form['team_details']['away_goals'] = array(
    '#title' => t('Away goals'),
    '#description' => t('The number of goals scored by the home team.'),
    '#type' => 'textfield',
    '#default_value' => isset($futbol_match->away_goals) ? $futbol_match->away_goals : '',
  );
  $form['team_details']['away_team_id'] = array(
    '#title' => t('Away team'),
    '#description' => t("The match's away team."),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#autocomplete_path' => 'futbol/team/autocomplete',
    '#default_value' => isset($futbol_match->away_team_id) ? futbol_team_autocomplete_val($futbol_match->away_team_id) : '',
  );
  $form['date_details'] = array(
    '#title' => t('Match date'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['date_details']['match_date'] = array(
    '#type' => 'date_popup',
    '#default_value' => isset($futbol_match->match_date) ? date('Y-m-d H:i:s', $futbol_match->match_date) : '',
  );
  $form['competition_details'] = array(
    '#title' => t('Competition details'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['competition_details']['ciid'] = array(
    '#title' => t('Competition'),
    '#description' => t('The competition to which the match belongs.'),
    '#type' => 'textfield',
    '#autocomplete_path' => 'futbol/competition/autocomplete',
    //'#default_value' => isset($futbol_match->ciid) ? futbol_competition_autocomplete_val($futbol_match->ciid) : '',
    '#default_value' => isset($futbol_match->ciid) ? $futbol_match->ciid : '',
  );
  $form['#validate'][] = 'futbol_match_form_validate';
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save team'),
  );
  return $form;
}

/**
 * Validation function for the match form.
 */
function futbol_match_form_validate($form, $form_state) {
  $values = $form_state['values'];
  // Make sure both goals fields have values.
  if ((rempty($values['home_goals']) && !rempty($values['away_goals'])) || (!rempty($values['home_goals']) && rempty($values['away_goals']))) {
    form_set_error('form', t('Only the @team field has a value. Both of the home goals and away goals fields should be blank or have values.', array('@team' => rempty($values['away_goals']) ? 'home goals' : 'away goals')));
  }
  // Check if the goals fields are integers (we can cast the goals to ints because empty is 0 anyway).
  if ((!rempty($values['home_goals']) && !is_int((int) $values['home_goals'])) || (!rempty($values['away_goals']) && !is_int((int) $values['away_goals']))) {
    form_set_error('form', t('The goals fields must either be blank or contain integer numbers only.'));
  }
  // Ensure the team fields are actual teams.
  $home_id = futbol_load_team_by_id(futbol_autocomplete_filter($values['home_team_id']));
  $away_id = futbol_load_team_by_id(futbol_autocomplete_filter($values['away_team_id']));
  if (empty($home_id) || empty($away_id)) {
    form_set_error('form', t('The @team team could not be located.', array('@team' => empty($home_id) ? 'home' : 'away')));
  }
}

/**
 * Submit callback for the match form.
 */
function futbol_match_form_submit(&$form, &$form_state) {
  $futbol_match = entity_ui_form_submit_build_entity($form, $form_state);
  // Change a few values.
  $futbol_match->home_team_id = futbol_autocomplete_filter($futbol_match->home_team_id);
  $futbol_match->away_team_id = futbol_autocomplete_filter($futbol_match->away_team_id);
  $futbol_match->match_date = futbol_date($futbol_match->match_date);
  $futbol_match->save();
  drupal_set_message(t('Match %match added.', array('%match' => futbol_match_label($futbol_match))));
  $form_state['redirect'] = 'admin/futbol/match';
}

/**
 * Loads an Futbol match by the match ID.
 */
function futbol_match_load($mid = NULL, $reset = FALSE) {
  $mids = (isset($mid) ? array($mid) : array());
  $match = futbol_match_load_multiple($mids, $reset);
  return $match ? reset($match) : FALSE;
}

/**
 * Loads multiple matches by ID or based on a set of matching conditions.
 *
 * @see entity_load()
 *
 * @param $mids
 *   An array of match IDs.
 * @param $conditions
 *   An array of conditions on the futbol_team table in the form
 *     'field' => $value.
 * @param $reset
 *   Whether to reset the internal team loading cache.
 *
 * @return
 *   An array of team objects indexed by mid.
 */
function futbol_match_load_multiple($mids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('match', $mids, $conditions, $reset);
}

/**
 * Generates the general config form.
 */
function futbol_config_form($form, &$form_state) {
  $form['competition'] = array(
    '#type' => 'fieldset',
    '#title' => t('Competition rules'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['competition']['points_per_win'] = array(
    '#type' => 'textfield',
    '#title' => t('Points for winning a game'),
    '#default_value' => variable_get('points_per_win', 3),
    '#size' => 2,
    '#maxlength' => 1,
  );
  $form['competition']['points_per_draw'] = array(
    '#type' => 'textfield',
    '#title' => t('Points for drawing a game'),
    '#default_value' => variable_get('points_per_draw', 1),
    '#size' => 2,
    '#maxlength' => 1,
  );
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['general']['default_country'] = array(
    '#type' => 'select',
    '#title' => t('Default country'),
    '#options' => futbol_list_countries(),
    '#default_value' => variable_get('futbol_default_country', 'GB'),
  );
  $form['general']['default_currency'] = array(
    '#type' => 'select',
    '#title' => t('Default currency'),
    '#options' => futbol_list_currencies(),
    '#default_value' => variable_get('futbol_default_currency', 'GBP'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Autocomplete results for the Futbol Team entity.
 */
function _futbol_team_autocomplete($string) {
  $matches = array();
  // Select the team table.
  $query = db_select('futbol_team', 't');
  // We want to query the 3 name fields, so select them.
  $query
  ->fields('t', array('tid', 'common_name', 'short_name', 'full_name'))
  ->range(0, 10);
  // Now implement an 'or' object.
  $or = db_or();
  $or->condition('t.common_name', '%' . db_like($string) . '%', 'LIKE');
  $or->condition('t.short_name', '%' . db_like($string) . '%', 'LIKE');
  $or->condition('t.full_name', '%' . db_like($string) . '%', 'LIKE');
  $query->condition($or);
  // Execute the query.
  $return = $query->execute();
  // Add matches to $matches .
  foreach ($return as $row) {
    $string = check_plain($row->common_name) . ' (' . $row->tid . ')';
    $matches[$string] = $string;
  }
  // Return for JS.
  drupal_json_output($matches);
}

/**
 * Returns the text from between parenthesis ().
 */
function futbol_autocomplete_filter($string) {
  preg_match('#\((.*?)\)#', $string, $match);
  return !empty($match[1]) ? $match[1] : $string;
}

/**
 * Load a Futbol Team by ID.
 */
function futbol_load_team_by_id($id) {
  $query = db_select('futbol_team', 't');
  $query->fields('t', array('common_name'))
  ->condition('t.tid', $id, '=');
  return $query->execute()->fetchField();
}

/**
 * Provide an autocomplete string for a Team field.
 */
function futbol_team_autocomplete_val($id) {
  $name = futbol_load_team_by_id($id);
  return $name . ' (' . $id . ')';
}

/**
 * Exactly the same as empty except the integer 0 isn't counted
 * as empty.
 */
function rempty($var) {
  if ($var === 0) {
    return FALSE;
  }
  return empty($var);
}

/**
 * Returns a list of currencies keyed by their currency codes.
 */
function futbol_list_currencies() {
  $currencies = array(
    'AFN' => t('Afghanistani Afghani'),
    'ALL' => t('Albanian Lek'),
    'DZD' => t('Algerian Dinar'),
    'ARS' => t('Argentine Peso'),
    'AWG' => t('Aruba Florin'),
    'AUD' => t('Australian Dollar'),
    'AZN' => t('Azerbaijan New Maneat'),
    'BSD' => t('Bahamian Dollar'),
    'BHD' => t('Bahraini Dinar'),
    'BDT' => t('Bangladeshi Taka'),
    'BBD' => t('Barbadian Dollar'),
    'BYR' => t('Belarus Ruble'),
    'BZD' => t('Belize Dollar'),
    'BMD' => t('Bermuda Dollar'),
    'BTN' => t('Bhutanese Ngultrum'),
    'BOB' => t('Bolivian Boliviano'),
    'BAM' => t('Bosnia and Herzegovina Convertible Marka'),
    'BWP' => t('Botswana Pula'),
    'BRL' => t('Brazilian Real'),
    'GBP' => t('British Pound'),
    'BND' => t('Brunei Dollar'),
    'BGN' => t('Bulgarian Lev'),
    'BIF' => t('Burundi Franc'),
    'KHR' => t('Cambodia Riel'),
    'CAD' => t('Canadian Dollar'),
    'CVE' => t('Cape Verdean Escudo'),
    'KYD' => t('Cayman Islands Dollar'),
    'XOF' => t('CFA Franc'),
    'XAF' => t('CFA Franc'),
    'CLP' => t('Chilean Peso'),
    'CNY' => t('Chinese Yuan'),
    'COP' => t('Colombian Peso'),
    'KMF' => t('Comoros Franc'),
    'CRC' => t('Costa Rica Colon'),
    'HRK' => t('Croatian Kuna'),
    'CUP' => t('Cuban Peso'),
    'CYP' => t('Cyprus Pound'),
    'CZK' => t('Czech Koruna'),
    'DKK' => t('Danish Krone'),
    'DJF' => t('Dijiboutian Franc'),
    'DOP' => t('Dominican Peso'),
    'XCD' => t('East Caribbean Dollar'),
    'EGP' => t('Egyptian Pound'),
    'SVC' => t('El Salvador Colon'),
    'ERN' => t('Eritrean Nakfa'),
    'EEK' => t('Estonian Kroon'),
    'ETB' => t('Ethiopian Birr'),
    'EUR' => t('Euro'),
    'FKP' => t('Falkland Islands Pound'),
    'FJD' => t('Fiji Dollar'),
    'GMD' => t('Gambian Dalasi'),
    'GHC' => t('Ghanian Cedi'),
    'GIP' => t('Gibraltar Pound'),
    'XAU' => t('Gold Ounces'),
    'GTQ' => t('Guatemala Quetzal'),
    'GGP' => t('Guernsey Pound'),
    'GNF' => t('Guinea Franc'),
    'GYD' => t('Guyana Dollar'),
    'HTG' => t('Haiti Gourde'),
    'HNL' => t('Honduras Lempira'),
    'HKD' => t('Hong Kong Dollar'),
    'HUF' => t('Hungarian Forint'),
    'ISK' => t('Iceland Krona'),
    'INR' => t('Indian Rupee'),
    'IDR' => t('Indonesian Rupiah'),
    'IRR' => t('Iran Rial'),
    'IQD' => t('Iraqi Dinar'),
    'ILS' => t('Israeli Shekel'),
    'JMD' => t('Jamaican Dollar'),
    'JPY' => t('Japanese Yen'),
    'JOD' => t('Jordanian Dinar'),
    'KZT' => t('Kazakhstan Tenge'),
    'KES' => t('Kenyan Shilling'),
    'KRW' => t('Korean Won'),
    'KWD' => t('Kuwaiti Dinar'),
    'KGS' => t('Kyrgyzstan Som'),
    'LAK' => t('Lao Kip'),
    'LVL' => t('Latvian Lat'),
    'LBP' => t('Lebanese Pound'),
    'LSL' => t('Lesotho Loti'),
    'LRD' => t('Liberian Dollar'),
    'LYD' => t('Libyan Dinar'),
    'LTL' => t('Lithuanian Lita'),
    'MOP' => t('Macau Pataca'),
    'MKD' => t('Macedonian Denar'),
    'MGA' => t('Malagasy ariary'),
    'MWK' => t('Malawian Kwacha'),
    'MYR' => t('Malaysian Ringgit'),
    'MVR' => t('Maldives Rufiyaa'),
    'MTL' => t('Maltese Lira'),
    'MRO' => t('Mauritania Ougulya'),
    'MUR' => t('Mauritius Rupee'),
    'MXN' => t('Mexican Peso'),
    'MDL' => t('Moldovan Leu'),
    'MNT' => t('Mongolian Tugrik'),
    'MAD' => t('Moroccan Dirham'),
    'MZM' => t('Mozambique Metical'),
    'MMK' => t('Myanmar Kyat'),
    'NAD' => t('Namibian Dollar'),
    'NPR' => t('Nepalese Rupee'),
    'ANG' => t('Neth Antilles Guilder'),
    'NZD' => t('New Zealand Dollar'),
    'NIO' => t('Nicaragua Cordoba'),
    'NGN' => t('Nigerian Naira'),
    'KPW' => t('North Korean Won'),
    'NOK' => t('Norwegian Krone'),
    'OMR' => t('Omani Rial'),
    'XPF' => t('Pacific Franc'),
    'PKR' => t('Pakistani Rupee'),
    'XPD' => t('Palladium Ounces'),
    'PAB' => t('Panama Balboa'),
    'PGK' => t('Papua New Guinea Kina'),
    'PYG' => t('Paraguayan Guarani'),
    'PEN' => t('Peruvian Nuevo Sol'),
    'PHP' => t('Philippine Peso'),
    'XPT' => t('Platinum Ounces'),
    'PLN' => t('Polish Zloty'),
    'QAR' => t('Qatar Rial'),
    'RON' => t('Romanian New Leu'),
    'RUB' => t('Russian Rouble'),
    'RWF' => t('Rwandese Franc'),
    'WST' => t('Samoan Tala'),
    'STD' => t('Sao Tome Dobra'),
    'SAR' => t('Saudi Arabian Riyal'),
    'SCR' => t('Seychelles Rupee'),
    'RSD' => t('Serbian Dinar'),
    'SLL' => t('Sierra Leone Leone'),
    'XAG' => t('Silver Ounces'),
    'SGD' => t('Singapore Dollar'),
    'SKK' => t('Slovak Koruna'),
    'SBD' => t('Solomon Islands Dollar'),
    'SOS' => t('Somali Shilling'),
    'ZAR' => t('South African Rand'),
    'LKR' => t('Sri Lanka Rupee'),
    'SHP' => t('St Helena Pound'),
    'SDG' => t('Sudanese Pound'),
    'SRD' => t('Surinam Dollar'),
    'SZL' => t('Swaziland Lilageni'),
    'SEK' => t('Swedish Krona'),
    'CHF' => t('Swiss Franc'),
    'SYP' => t('Syrian Pound'),
    'TWD' => t('Taiwan Dollar'),
    'TZS' => t('Tanzanian Shilling'),
    'THB' => t('Thai Baht'),
    'TOP' => t("Tonga Pa'anga"),
    'TTD' => t('Trinidad & Tobago Dollar'),
    'TND' => t('Tunisian Dinar'),
    'TRY' => t('Turkish Lira'),
    'USD' => t('U.S. Dollar'),
    'AED' => t('UAE Dirham'),
    'UGX' => t('Ugandan Shilling'),
    'UAH' => t('Ukraine Hryvnia'),
    'UYU' => t('Uruguayan New Peso'),
    'UZS' => t('Uzbekistan Sum'),
    'VUV' => t('Vanuatu Vatu'),
    'VEB' => t('Venezuelan Bolivar'),
    'VND' => t('Vietnam Dong'),
    'YER' => t('Yemen Riyal'),
    'YUM' => t('Yugoslav Dinar'),
    'ZMK' => t('Zambian Kwacha'),
    'ZWD' => t('Zimbabwe Dollar'),
  );
  return $currencies;
}

/**
 * Returns an array of country codes and country names.
 *
 * @ Todo: Include non-official states such as Catalunya and Scotland.
 *
 */
function futbol_list_countries() {
  $countries = array(
    'AD' => 'Andorra',
    'AE' => 'United Arab Emirates',
    'AF' => 'Afghanistan',
    'AG' => 'Antigua and Barbuda',
    'AI' => 'Anguilla',
    'AL' => 'Albania',
    'AM' => 'Armenia',
    'AO' => 'Angola',
    'AQ' => 'Antarctica',
    'AR' => 'Argentina',
    'AS' => 'American Samoa',
    'AT' => 'Austria',
    'AU' => 'Australia',
    'AW' => 'Aruba',
    'AX' => 'Åland Islands',
    'AZ' => 'Azerbaijan',
    'BA' => 'Bosnia and Herzegovina',
    'BB' => 'Barbados',
    'BD' => 'Bangladesh',
    'BE' => 'Belgium',
    'BF' => 'Burkina Faso',
    'BG' => 'Bulgaria',
    'BH' => 'Bahrain',
    'BI' => 'Burundi',
    'BJ' => 'Benin',
    'BL' => 'Saint Barthélemy',
    'BM' => 'Bermuda',
    'BN' => 'Brunei Darussalam',
    'BO' => 'Bolivia',
    'BQ' => 'Bonaire, Sint Eustatius and Saba',
    'BR' => 'Brazil',
    'BS' => 'Bahamas',
    'BT' => 'Bhutan',
    'BV' => 'Bouvet Island',
    'BW' => 'Botswana',
    'BY' => 'Belarus',
    'BZ' => 'Belize',
    'CA' => 'Canada',
    'CC' => 'Cocos (Keeling) Islands',
    'CD' => 'Congo, the Democratic Republic of the',
    'CF' => 'Central African Republic',
    'CG' => 'Congo',
    'CH' => 'Switzerland',
    'CI' => "Côte d'Ivoire",
    'CK' => 'Cook Islands',
    'CL' => 'Chile',
    'CM' => 'Cameroon',
    'CN' => 'China',
    'CO' => 'Colombia',
    'CR' => 'Costa Rica',
    'CU' => 'Cuba',
    'CV' => 'Cape Verde',
    'CW' => 'Curaçao',
    'CX' => 'Christmas Island',
    'CY' => 'Cyprus',
    'CZ' => 'Czech Republic',
    'DE' => 'Germany',
    'DJ' => 'Djibouti',
    'DK' => 'Denmark',
    'DM' => 'Dominica',
    'DO' => 'Dominican Republic',
    'DZ' => 'Algeria',
    'EC' => 'Ecuador',
    'EE' => 'Estonia',
    'EG' => 'Egypt',
    'EH' => 'Western Sahara',
    'ER' => 'Eritrea',
    'ES' => 'Spain',
    'ET' => 'Ethiopia',
    'FI' => 'Finland',
    'FJ' => 'Fiji',
    'FK' => 'Falkland Islands (Malvinas)',
    'FM' => 'Micronesia, Federated States of',
    'FO' => 'Faroe Islands',
    'FR' => 'France',
    'GA' => 'Gabon',
    'GB' => 'United Kingdom',
    'GD' => 'Grenada',
    'GE' => 'Georgia',
    'GF' => 'French Guiana',
    'GG' => 'Guernsey',
    'GH' => 'Ghana',
    'GI' => 'Gibraltar',
    'GL' => 'Greenland',
    'GM' => 'Gambia',
    'GN' => 'Guinea',
    'GP' => 'Guadeloupe',
    'GQ' => 'Equatorial Guinea',
    'GR' => 'Greece',
    'GS' => 'South Georgia and the South Sandwich Islands',
    'GT' => 'Guatemala',
    'GU' => 'Guam',
    'GW' => 'Guinea-Bissau',
    'GY' => 'Guyana',
    'HK' => 'Hong Kong',
    'HM' => 'Heard Island and McDonald Islands',
    'HN' => 'Honduras',
    'HR' => 'Croatia',
    'HT' => 'Haiti',
    'HU' => 'Hungary',
    'ID' => 'Indonesia',
    'IE' => 'Ireland',
    'IL' => 'Israel',
    'IM' => 'Isle of Man',
    'IN' => 'India',
    'IO' => 'British Indian Ocean Territory',
    'IQ' => 'Iraq',
    'IR' => 'Iran, Islamic Republic of',
    'IS' => 'Iceland',
    'IT' => 'Italy',
    'JE' => 'Jersey',
    'JM' => 'Jamaica',
    'JO' => 'Jordan',
    'JP' => 'Japan',
    'KE' => 'Kenya',
    'KG' => 'Kyrgyzstan',
    'KH' => 'Cambodia',
    'KI' => 'Kiribati',
    'KM' => 'Comoros',
    'KN' => 'Saint Kitts and Nevis',
    'KP' => "Korea, Democratic People's Republic of",
    'KR' => 'Korea, Republic of',
    'KW' => 'Kuwait',
    'KY' => 'Cayman Islands',
    'KZ' => 'Kazakhstan',
    'LA' => "Lao People's Democratic Republic",
    'LB' => 'Lebanon',
    'LC' => 'Saint Lucia',
    'LI' => 'Liechtenstein',
    'LK' => 'Sri Lanka',
    'LR' => 'Liberia',
    'LS' => 'Lesotho',
    'LT' => 'Lithuania',
    'LU' => 'Luxembourg',
    'LV' => 'Latvia',
    'LY' => 'Libya',
    'MA' => 'Morocco',
    'MC' => 'Monaco',
    'MD' => 'Moldova, Republic of',
    'ME' => 'Montenegro',
    'MF' => 'Saint Martin (French part)',
    'MG' => 'Madagascar',
    'MH' => 'Marshall Islands',
    'MK' => 'Macedonia, the former Yugoslav Republic of',
    'ML' => 'Mali',
    'MM' => 'Myanmar',
    'MN' => 'Mongolia',
    'MO' => 'Macao',
    'MP' => 'Northern Mariana Islands',
    'MQ' => 'Martinique',
    'MR' => 'Mauritania',
    'MS' => 'Montserrat',
    'MT' => 'Malta',
    'MU' => 'Mauritius',
    'MV' => 'Maldives',
    'MW' => 'Malawi',
    'MX' => 'Mexico',
    'MY' => 'Malaysia',
    'MZ' => 'Mozambique',
    'NA' => 'Namibia',
    'NC' => 'New Caledonia',
    'NE' => 'Niger',
    'NF' => 'Norfolk Island',
    'NG' => 'Nigeria',
    'NI' => 'Nicaragua',
    'NL' => 'Netherlands',
    'NO' => 'Norway',
    'NP' => 'Nepal',
    'NR' => 'Nauru',
    'NU' => 'Niue',
    'NZ' => 'New Zealand',
    'OM' => 'Oman',
    'PA' => 'Panama',
    'PE' => 'Peru',
    'PF' => 'French Polynesia',
    'PG' => 'Papua New Guinea',
    'PH' => 'Philippines',
    'PK' => 'Pakistan',
    'PL' => 'Poland',
    'PM' => 'Saint Pierre and Miquelon',
    'PN' => 'Pitcairn',
    'PR' => 'Puerto Rico',
    'PS' => 'Palestine, State of',
    'PT' => 'Portugal',
    'PW' => 'Palau',
    'PY' => 'Paraguay',
    'QA' => 'Qatar',
    'RE' => 'Réunion',
    'RO' => 'Romania',
    'RS' => 'Serbia',
    'RU' => 'Russian Federation',
    'RW' => 'Rwanda',
    'SA' => 'Saudi Arabia',
    'SB' => 'Solomon Islands',
    'SC' => 'Seychelles',
    'SD' => 'Sudan',
    'SE' => 'Sweden',
    'SG' => 'Singapore',
    'SH' => 'Saint Helena, Ascension and Tristan da Cunha',
    'SI' => 'Slovenia',
    'SJ' => 'Svalbard and Jan Mayen',
    'SK' => 'Slovakia',
    'SL' => 'Sierra Leone',
    'SM' => 'San Marino',
    'SN' => 'Senegal',
    'SO' => 'Somalia',
    'SR' => 'Suriname',
    'SS' => 'South Sudan',
    'ST' => 'Sao Tome and Principe',
    'SV' => 'El Salvador',
    'SX' => 'Sint Maarten (Dutch part)',
    'SY' => 'Syrian Arab Republic',
    'SZ' => 'Swaziland',
    'TC' => 'Turks and Caicos Islands',
    'TD' => 'Chad',
    'TF' => 'French Southern Territories',
    'TG' => 'Togo',
    'TH' => 'Thailand',
    'TJ' => 'Tajikistan',
    'TK' => 'Tokelau',
    'TL' => 'Timor-Leste',
    'TM' => 'Turkmenistan',
    'TN' => 'Tunisia',
    'TO' => 'Tonga',
    'TR' => 'Turkey',
    'TT' => 'Trinidad and Tobago',
    'TV' => 'Tuvalu',
    'TW' => 'Taiwan, Province of China',
    'TZ' => 'Tanzania, United Republic of',
    'UA' => 'Ukraine',
    'UG' => 'Uganda',
    'UM' => 'United States Minor Outlying Islands',
    'US' => 'United States',
    'UY' => 'Uruguay',
    'UZ' => 'Uzbekistan',
    'VA' => 'Holy See (Vatican City State)',
    'VC' => 'Saint Vincent and the Grenadines',
    'VE' => 'Venezuela, Bolivarian Republic of',
    'VG' => 'Virgin Islands, British',
    'VI' => 'Virgin Islands, U.S.',
    'VN' => 'Viet Nam',
    'VU' => 'Vanuatu',
    'WF' => 'Wallis and Futuna',
    'WS' => 'Samoa',
    'YE' => 'Yemen',
    'YT' => 'Mayotte',
    'ZA' => 'South Africa',
    'ZM' => 'Zambia',
    'ZW' => 'Zimbabwe',
  );
  // Sort the array by the country name.
  asort($countries);
  return $countries;
}

/**
 * Takes a date in the format Y-m-d H:i and returns a timestamp.
 */
function futbol_date($date) {
  $d = DateTime::createFromFormat('Y-m-d H:i', $date);
  return $d->getTimestamp();
}
